module.exports.Survey = {
  name: 'Survey',
  fields: {
    id: 'serial primary key',
    name: 'varchar(500)',
  },
  values: [
    {
      name: 'Survey of Traffic Signs in Hong Kong'
    }
  ]
}
