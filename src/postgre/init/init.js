var { to } = require('../../utility/func')
var { sql } = require('../sql')
var { Record } = require('./table/Record')
var { Survey } = require('./table/Survey')
var { Report } = require('./table/Report')

var err, data
module.exports.init = async db => {
  db.postgre = await db.Postgre.pool('postgres');
  [err, data] = await to(db.postgre.query('drop schema survey cascade;'));
  [err, data] = await to(db.postgre.query('create schema survey;'));
  [err, data] = await to(db.postgre.query('SET search_path TO \'survey\';'));
  [err, data] = await create(db, [ Record, Survey, Report ]);
  return [err, data]
}

const create = async (db, tables) => {
  for(var table of tables){
    [err, data] = await to(db.postgre.query(sql.create(table.name, table.fields)));
    [err, data] = await to(db.postgre.query(table.values.map(value => sql.insert(table.name ,value)).join(';')));
  }
  return [err, data]
}
