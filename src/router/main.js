var fs = require('fs')
var { to } = require('../utility/func')
var { init } = require('../postgre/init/init')
var { submit } = require('../utility/submit')
var { report } = require('../utility/report')

var err, data
module.exports.main = (app, db) => {

  app.post('/report', async (req, res) => {
    [err, data] = await report(db, req.body)
    res.json({ result: err? err: data })
  })

  app.post('/delete/:id', async (req, res) => {
    db.postgre = await db.Postgre.pool('postgres');
    const query = 'delete from \"Record\" where \"id\"=' + req.params.id + ';';
    [err, data] = await to(db.postgre.query(query))
    res.json({ result: err? err: data })
  })

  app.post('/record', async (req, res) => {
    db.postgre = await db.Postgre.pool('postgres');
    [err, data] = await to(db.postgre.query('select * from \"Record\"'))
    res.json({ result: err? err: data.rows })
  })

  app.post('/submit', async (req, res) => {
    [err, data] = await submit(db, req.body)
    res.json({ result: err? err: data })
  })

  app.post('/init', async (req, res) => {
    [err, data] = await init(db)
    res.json({ result: err? err: data })
  })

  app.get('/:page/:file', async (req, res) => {
    const { page, file } = req.params
    const type = file.split('.')[1]
    res.writeHead(200, { 'Content-Type': 'text/' + type })
    fs.readFile('./src/' + page + '/' + file, null, (err, data) => {
      res.write(err? 'Invalid url': data)
      res.end()
    })
  })

}
