var { to } = require('./func')
var { sql } = require('../postgre/sql')

var err, data
module.exports.submit = async (db, { survey, values }) => {
  db.postgre = await db.Postgre.pool('postgres')
  var pairs = { survey, timestamp: Date.now() }
  values.map(([id, value]) => pairs[id] = value)
  const query = sql.insert('Record', pairs);
  [err, data] = await to(db.postgre.query(query))
  return [err, data]
}
