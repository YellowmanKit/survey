var { to } = require('./func')
var { sql } = require('../postgre/sql')

var err, data
module.exports.report = async (db, { address }) => {
  db.postgre = await db.Postgre.pool('postgres')
  const query = sql.insert('Report', { address });
  [err, data] = await to(db.postgre.query(query))
  return [err, data]
}
