require('dotenv').config()
var express = require('express')
const app = express()
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
var { Postgre } = require('./postgre/postgre')

var { main } = require('./router/main')
main(app, { Postgre })

var http = require('http')
app.server = http.createServer(app).listen(1025,
  ()=>{ console.log('survey is running at port 1025') } )
